/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package car;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author user
 */
public class Serialization {
static final String filePath = "user.ser";
    static void serialize(Car car){
    try {
        FileOutputStream fos = new FileOutputStream(filePath);
        ObjectOutputStream outputStream = new ObjectOutputStream(fos);
        outputStream.writeObject(car);
        outputStream.close();
    } catch (IOException ex) {
        System.err.println(ex);
    }
    }
    static Car deserialize() {
    Car savedCar = null;
 
    try {
        FileInputStream fis = new FileInputStream(filePath);
        ObjectInputStream inputStream = new ObjectInputStream(fis);
        savedCar = (Car) inputStream.readObject();
        inputStream.close();
    } catch (IOException | ClassNotFoundException ex) {
        System.err.println(ex);
    }
 
    return savedCar;
    
    }
    public static void main(String[] args) {
        String model = "Dacia";
        double price = 25000;
         
        Car newCar = new Car(model,price);
         
        serialize(newCar);
 
        Car savedCar = deserialize();
         
        if (savedCar != null) {
            savedCar.print();
    }
    
}}
