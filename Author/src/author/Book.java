
package author;

import java.util.Arrays;


public class Book {
   public String name;
   public Author[] authors;
   public double price;
   public int qtyInStock;
    
    public Book(String name, Author[] authors, double price){
        this.name=name;
        this.authors=authors;
        this.price=price;
    }
    public Book(String name, Author author, double price, int qtyInStock){
        this.name=name;
        this.authors=authors;
        this.price=price;
        this.qtyInStock=qtyInStock;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public void setAuthors(Author[] authors) {
        this.authors = authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }
    public void printAuthors(){
       for (Author author : authors) {
           System.out.println(author);
       }
    }
   @Override
   public String toString(){
       
           return "The book "+name+" by author " + Arrays.toString(authors);
       }
       
    }
    

