/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package author;

public class Author {

    private String name;
    private String email;
    private char gender;

    public Author(String name, String email, char gender) {
        this.name=name;
        this.email=email;
        this.gender=gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    @Override
    public String toString(){
   return this.name+ " "+this.email+ " "+this.gender;
    }

    public static void main(String[] args) {
        Author autor= new Author("mihai Eminescu", "eminescu@gmail.com", 'm');
        System.out.println(autor);
        autor.setName("Mihai Eminescu");
        autor.setGender('m');
        autor.setEmail("eminescu@gmail.com");
        System.out.println(autor);
        Author[] authors=new Author[2];
        authors[0]= new Author("Mihai Eminescu","eminescu@yahoo.ro",'m');
        authors[1]=new Author("Blaga Lucian","lucian@yahoo.com",'m');
        Book book1=new Book("Luceafarul", authors, 45.0);
        System.out.println(book1);
     
       
    }

}
