/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

/**
 *
 * @author user
 */
public class Rectangle extends Shape {

    private double width;
    private double lenght;

    public Rectangle() {
        super();
        double width = 1.0;
        double lenght = 1.0;
    }

    public Rectangle(double width, double lenght) {
        super();
        this.width = width;
        this.lenght = lenght;
    }

    public Rectangle(double width, double lenght, String color, boolean filled) {
        super(color, filled);
        this.lenght = lenght;
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLenght() {
        return lenght;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    public double getArea() {
        return lenght * width;
    }

    public double getPerimeter() {
        return 2 * (lenght + width);
    }

    public String toString() {
        return " A Rectangle with width " + width + " which is a subclass of " ;
    }
}
