/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

abstract public class Shape {

    private String color;
    boolean filled;
    public double radius;

    public Shape() {
        color = "red";
        filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {

    }

    public boolean isFilled() {
        return true;
    }

    public void setFilled(boolean filled) {
    }

    public double getArea() {
        return radius * radius * Math.PI;

    }

    public double getPerimeter() {
        return Math.PI * (radius * 2);
    }

    public String toString() {
        return "A shape with color " + color + " is " + filled;
    }

    public static void main(String[] args) {
        Circle c1 = new Circle();
        System.out.println(c1);
        Shape s1 = new Circle(5.5, "red", false);
        System.out.println(s1);
        System.out.println(s1.getArea());
        System.out.println(s1.getPerimeter());
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());
    }

}
