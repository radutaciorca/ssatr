/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

public class Square extends Rectangle {

    private double side;

    public Square() {
        super();
    }

    public Square(double side) {
        super();

    }

    public Square(double side, String color, boolean filled) {
        super(side,side);
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    @Override
   public String toString(){
    return "A Square with side= "+side+" which is a sublass of "+super.toString();
    }

}


