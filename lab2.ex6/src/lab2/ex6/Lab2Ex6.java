package lab2.ex6;

public class Lab2Ex6 {

    static int factorial(int n) {
        int res = 1, i;
        for (i = 2; i <= n; i++) {
            res *= i;
        }
        return res;
    }

    static int factor(int n) {
        if (n == 0) {
            return 1;
        }

        return n * factorial(n - 1);
    }

    public static void main(String[] args) {
        int nume = 5;

        factorial(nume);
        factor(nume);
        System.out.println(factorial(nume) + " ");
        System.out.println(factor(nume) + " ");
    }

}
