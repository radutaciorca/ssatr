/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffemachine;

/**
 *
 * @author user
 */
public class CofeeDrinker {
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException, NumberofCoffeException{
            if(c.getTemp()>60)
                  throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
            if(c.getConc()>50)
                  throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");    
            if(c.getnrOfCofee()>10)
                throw new NumberofCoffeException(c.getnrOfCofee(),"You had too many cofees");
            System.out.println("Drink cofee:"+c);
      }
}
