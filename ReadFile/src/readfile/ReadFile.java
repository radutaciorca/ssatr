
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class ReadFile {

    private BufferedReader input;
    private String line;
    private JFileChooser fc;

    public ReadFile() {
        line = new String();
        fc = new JFileChooser();
    }

    private void displayGUI() {
        final JFrame frame = new JFrame("Read File");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JTextArea tarea = new JTextArea(10, 10);

        JButton readButton = new JButton("OPEN FILE");
        readButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                int returnVal = fc.showOpenDialog(frame);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    //This is where a real application would open the file.
                    try {
                        input = new BufferedReader(
                                new InputStreamReader(
                                        new FileInputStream(
                                                file)));
                        tarea.read(input, "READING FILE :-)");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("Operation is CANCELLED :(");
                }
            }
        });

        frame.getContentPane().add(tarea, BorderLayout.CENTER);
        frame.getContentPane().add(readButton, BorderLayout.PAGE_END);
        frame.pack();
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

    public static void main(String... args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new ReadFile().displayGUI();
            }
        });
    }
}
