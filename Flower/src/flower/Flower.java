package flower;

public class Flower {

    static int noOfObjects = 0;
    int petal;

    Flower() {
        System.out.println("Flower has been created!");
        noOfObjects += 1;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for (int i = 0; i < 5; i++) {
            Flower f = new Flower();
            garden[i] = f;
System.out.println(Flower.noOfObjects);
        }
    }

}
