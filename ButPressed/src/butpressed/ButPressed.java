/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package butpressed;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author user
 */
public class ButPressed  extends Frame implements ActionListener {
   private Label lblCount;     
   private TextField tfCount;  
   private Button btnCount; 
   private int count = 0;   
 
   public ButPressed () {
      setLayout(new FlowLayout());
 
      lblCount = new Label("Ai apasat"); 
      add(lblCount);                  
 
      tfCount = new TextField(count + "", 10); 
      tfCount.setEditable(false);      
      add(tfCount);                     
 
      btnCount = new Button("Apasa");  
      add(btnCount);                    
      btnCount.addActionListener(this);
        
 
      setSize(350, 300);       
      setTitle("Apasa butonul"); 
      setVisible(true);        
   }
 
  
   @Override
   public void actionPerformed(ActionEvent evt) {
      ++count;                     
      tfCount.setText(count + ""); 
                                  
   }
 
   // The entry main() method
   public static void main(String[] args) {
      // Invoke the constructor by allocating an anonymous instance
      new ButPressed();
   }

}
