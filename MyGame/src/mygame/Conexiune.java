/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class Conexiune implements Runnable {

    private ObjectOutputStream out;
    private ObjectInputStream in;
    private boolean run;
    private Game game;

    public Conexiune(Game game, Socket s) {
        this.game = game;
        try {
            out = new ObjectOutputStream(s.getOutputStream());
            in = new ObjectInputStream(s.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(Conexiune.class.getName()).log(Level.SEVERE, null, ex);
        }
        new Thread(this).start();
    }

    @Override
    public void run() {
        run = true;
        while (run) {
            try {

                Object object = in.readObject();
                game.Received(object);
            } catch (EOFException | SocketException e) {
                run=false;
            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void sendMsg(Object obj) {
        try {
            out.reset();
            out.writeObject(obj);
            out.flush();
        } catch (IOException ex) {
            Logger.getLogger(Conexiune.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void close() throws IOException {
       
        in.close();
        out.close();
    }

}
