package mygame;

import java.io.IOException;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author user
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        int choise = Integer.parseInt(JOptionPane.showInputDialog("1 for server | 2 for client"));
        if (choise == 1) {
            new Server();
        } else if (choise == 2) {
            new Client();
        }

    }

}
