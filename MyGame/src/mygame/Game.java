/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import static com.sun.glass.ui.Cursor.setVisible;
import game.gui.GameWindow;
import game.gui.Window;
import javax.swing.JOptionPane;

/**
 *
 * @author user
 */
public abstract class Game {

    public static final int WIDTH = 600, HEIGHT = 600;
    public static final int FIELD_WIDTH = WIDTH / 3, FIELD_HEIGHT = HEIGHT / 3;

    public static final int gol = 0, jucator1 = 1, jucator2 = 2;

    int[][] fields;

    private Window window;
    protected GameWindow gameWindow;
    protected int jucCurent;
    protected int thisJucator;

    public Game(int thisJucator) {
        this.thisJucator = thisJucator;
        window = new Window(this, "MY Game", WIDTH, HEIGHT);
        gameWindow = new GameWindow(this);
        fields = new int[3][3];
        window.add(gameWindow);
        window.setVisible(true);
        jucCurent = Game.jucator1;

    }

    protected void arataCastigator(int castigator) {
        if (castigator == Game.gol) {
            JOptionPane.showMessageDialog(null, "Remiza");
        }else{
        JOptionPane.showMessageDialog(null, "Jucatorul  " + castigator + " a castigat !");
    }
    }
    protected boolean Randul() {
        if (thisJucator == jucCurent) {
            return true;
        } else {
            return false;
        }
    }

    public abstract void inputReceived(int x, int y);

    public abstract void Received(Object object);

    public abstract void close();

    public int[][] getFields() {
        return fields;
    }
}
