/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import game.pachete.ClientPlay;
import game.pachete.FinalJoc;
import game.pachete.Update;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class Server extends Game {

    private ServerSocket ss;
    private Socket s;
    private Conexiune conex;

    public Server() throws IOException {
        super(Game.jucator1);

        ss = new ServerSocket(55000);
        s = ss.accept();
        conex = new Conexiune(this, s);

    }

    @Override
    public void inputReceived(int x, int y) {
        if (Randul()) {
            UpdateCamp(x, y);
        }
       
    }

    @Override
    public void Received(Object object) {
        if (object instanceof ClientPlay) {
            ClientPlay pac = (ClientPlay) object;
            UpdateCamp(pac.getX(), pac.getY());
        }

    }

    private void UpdateCamp(int x, int y) {
        if (fields[x][y] == Game.gol) {
            fields[x][y] = jucCurent;

            if (jucCurent == Game.jucator1) {
                jucCurent = Game.jucator2;
            } else if (jucCurent == Game.jucator2) {
                jucCurent = Game.jucator1;
            }
            conex.sendMsg(new Update(fields, jucCurent));
            gameWindow.repaint();
            int castigator = Castigatorul();
            if (castigator != Game.gol) {
                finalJoc(castigator);
            } else {
                int gol = 0;

                for (int a = 0; a < 3; a++) {
                    for (int b = 0; b < 3; b++) {
                        if (fields[a][b] == Game.gol) {
                            gol++;
                        }
                    }
                }
                if (gol == 9) {
                    finalJoc(castigator);

                }
            }
        }
    }

    private void finalJoc(int castigator) {
        arataCastigator(castigator);
        conex.sendMsg(new FinalJoc(castigator));
    }

    private int Castigatorul() {
        for (int jucator = Game.jucator1; jucator <= Game.jucator2; jucator++) {
            for (int y = 0; y < 3; y++) {
                int count = 0;
                for (int x = 0; x < 3; x++) {
                    if (fields[x][y] == jucator) {
                        count++;
                    }
                }
                if (count == 3) {
                    return jucator;
                }
            }
            for (int x = 0; x < 3; x++) {
                int count = 0;
                for (int y = 0; y < 3; y++) {
                    if (fields[x][y] == jucator) {
                        count++;
                    }
                }
                if (count == 3) {
                    return jucator;
                }
            }
            int count = 0;
            for (int coord = 0; coord < 3; coord++) {
                if (fields[coord][coord] == jucator) {
                    count++;
                }
            }
            if (count == 3) {
                return jucator;
            }
            count = 0;
            for (int coord = 0; coord < 3; coord++) {
                if (fields[2 - coord][coord] == jucator) {
                    count++;
                }
            }
            if (count == 3) {
                return jucator;
            }

        }
        return Game.gol;
    }

    public void close() {
        try {
            conex.close();
            ss.close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
