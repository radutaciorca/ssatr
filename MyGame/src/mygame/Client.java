/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import game.pachete.ClientPlay;
import game.pachete.FinalJoc;
import game.pachete.Update;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class Client extends Game {

    private Socket s;
    private Conexiune conex;

    public Client() {
        super(Game.jucator2);
        try {
            s = new Socket("192.168.1.9", 55000);
            conex = new Conexiune(this, s);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void inputReceived(int x, int y) {
        if(Randul()){conex.sendMsg(new ClientPlay(x,y));
    }
    }
    @Override
    public void Received(Object obj) {
        if(obj instanceof Update){
            Update pac =(Update) obj;
        fields = pac.getFields();
        jucCurent = pac.getJucCurent();
        
    
         gameWindow.repaint();
        }
        
        else if(obj instanceof FinalJoc){
            FinalJoc pac=(FinalJoc) obj;
            arataCastigator(pac.getCastigator());
        }
        gameWindow.repaint();
    }

    @Override
    public void close() {
        try {
            conex.close();
            s.close();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
