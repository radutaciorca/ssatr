/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.res;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author user
 */
public class Resurse {

    public static BufferedImage[] letters;

    static {
        letters = new BufferedImage[2];
        letters[0] = loadImage("/blueX.png");
        letters[1] = loadImage("/redCircle.png");
    }

    private static BufferedImage loadImage(String path) {
        try {
            return ImageIO.read(Class.class.getResource(path));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return null;
    }

}
