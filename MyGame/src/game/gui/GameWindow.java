/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.gui;

import game.res.Resurse;
import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;
import mygame.Game;

/**
 *
 * @author user
 */
public class GameWindow extends JPanel {

    private static final long serialVersionUID = 2767113216806410931L;
    private Game game;

    public GameWindow(Game game) {
        this.game = game;
        addMouseListener(new Input());
    }

    public void paint(Graphics g) {
        super.paint(g);

        Graphics2D g2D = (Graphics2D) g;
        g2D.setStroke(new BasicStroke(10));

        for (int x = Game.FIELD_WIDTH; x <= Game.FIELD_WIDTH * 2; x += Game.FIELD_WIDTH) {
            g2D.drawLine(x, 0, x, Game.HEIGHT);
        }
        for (int y = Game.FIELD_HEIGHT; y <= Game.FIELD_HEIGHT * 2; y += Game.FIELD_HEIGHT) {
            g2D.drawLine(0, y, Game.WIDTH, y);
        }
        for (int x = 0; x < 3; x++) {

            for (int y = 0; y < 3; y++) {
                int field = game.getFields()[x][y];
                if (field != Game.gol) {
                    g2D.drawImage(Resurse.letters[field - 1], x * Game.FIELD_WIDTH, y * Game.FIELD_HEIGHT, Game.FIELD_WIDTH, Game.FIELD_HEIGHT, null);
                }
            }
        }
    }

    class Input extends MouseAdapter {

        public void mousePressed(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                game.inputReceived(e.getX()/Game.FIELD_WIDTH, e.getY()/Game.FIELD_HEIGHT);
            }
        }

    }
}
