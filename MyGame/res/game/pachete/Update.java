/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.pachete;

import java.io.Serializable;

public class Update implements Serializable {

    private static final long serialVersionUID = 8609245006220470192L;
    private int[][] fields;
    private int jucCurent;

    public Update(int[][] fields, int jucCurent) {
        this.fields = fields;
        this.jucCurent = jucCurent;

    }

    public int[][] getFields() {
        return fields;
    }

    public int getJucCurent() {
        return jucCurent;
    }

}
