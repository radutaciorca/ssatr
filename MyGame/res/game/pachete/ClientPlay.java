/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.pachete;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class ClientPlay implements Serializable {

    private static final long serialVersionUID = 6007641566528131574L;
    private int x;
    private int y;

    public ClientPlay(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
