/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package joc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ServerJob extends Thread {

    private int flip = 0;
    private int flipResult = 0;
    public String userChoise;
    public int userChois;
    private String play;
    private int nrCastig = 0;
    Random r = new Random();
    private static Socket soc;
    private final Server server;

    public ServerJob(Server server, Socket soc) {
        this.soc = soc;
        this.server = server;
    }

    public void run() {
        try {
            OutputStream out = soc.getOutputStream();
            InputStream in = soc.getInputStream();
            BufferedReader read = new BufferedReader(new InputStreamReader(in));
            try {
                String name = "Buna, cum te numesti?";
                out.write(name.getBytes());
                String nam;
                nam = read.readLine();
                String greet = " Bine ai venit " + nam + " ! ";
                out.write(greet.getBytes());
                do {
                    String alege = "Alege 1 pentru Cap, 2 pentru Pajura";
                    out.write(alege.getBytes());

                    userChoise = read.readLine().trim();

                    while (!userChoise.equalsIgnoreCase("1") && !userChoise.equalsIgnoreCase("2")) {
                        String a = "Alegerea nu este corecta. Incearca din nou";
                        String b = "Alege 1 pentru Cap, 2 pentru Pajura";
                        out.write(a.getBytes());
                        out.write(b.getBytes());

                        userChoise = read.readLine();
                        userChois = Integer.parseInt(userChoise);
                    }
                    flip = r.nextInt(2) + 1;
                    flipResult++;

                    if (userChois == flip) {
                        String c = "Ai castigat !";
                        out.write(c.getBytes());
                        nrCastig++;
                    } else {
                        String d = "Ai pierdut !";
                        out.write(d.getBytes());
                    }

                    String e = "Joaca din nou? Y sau N ";
                    out.write(e.getBytes());
                    play = read.readLine();
                    while (!play.equalsIgnoreCase("Y") && !play.equalsIgnoreCase("N")) {
                        String f = "Raspuns gresit. Incearca din nou.";
                        String g = "Joaca din nou? Y sau N ";
                        out.write(f.getBytes());
                        out.write(g.getBytes());
                        play = read.readLine();
                    }
                } while (play.equalsIgnoreCase(
                        "Y"));

                String h = "Ai castigat de  " + nrCastig + " ori";
                out.write(h.getBytes());
            } catch (IOException ex) {
                Logger.getLogger(ServerJob.class.getName()).log(Level.SEVERE, null, ex);
            };
        } catch (IOException ex) {
            Logger.getLogger(ServerJob.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
